# docker-dummy

A Docker null-op image made from the no-op command "FROM scratch" with label "deprecated".

It can be used as a poor-man's method to overwrite other images, enabling them to be identified as to be purged by another job.

This repository is hosted on Gitlab (https://gitlab.com/jlecomte/images/docker-dummy), mirrored onto Github (https://github.com/julien-lecomte/docker-dummy) and deployed via Docker hub.

## Sample usage

~~~
# Get the image
docker pull julienlecomte/docker-dummy

# Tag an image you wish to "squash"
docker tag julienlecomte/docker-dummy adobe/flash:latest

# Push the "new" image, rendering the previous one dangling and available for a purge
docker push adobe/flash:latest

# List all images that are marked as deprecated:
docker images --filter label="deprecated=true"
~~~

